'use strict';
module.exports = (sequelize, DataTypes) => {
  const Card = sequelize.define('Card', {
    name: DataTypes.STRING,
    elixir: DataTypes.INTEGER,
    maxLevel: DataTypes.INTEGER,
    iconUrl: DataTypes.STRING
  }, {});
  Card.associate = function (models) {
    // associations can be defined here
  };
  return Card;
};