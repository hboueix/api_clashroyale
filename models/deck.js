'use strict';
module.exports = (sequelize, DataTypes) => {
  const Deck = sequelize.define('Deck', {
    title: DataTypes.STRING,
    elixirAvr: DataTypes.FLOAT,
    userId: DataTypes.INTEGER,
    cards: {
      type: DataTypes.STRING,
      async get() {
        const cards = this.getDataValue('cards').split(';')
        let res = []
        await cards.forEach(card => {
          res.push(JSON.parse(card))
        });
        return res
      },
      set(val) {
        this.setDataValue('cards', val.join(';'))
      }
    }
  }, {});
  Deck.associate = function (models) {
    //Deck.hasMany(models.Card)
    Deck.belongsTo(models.User, {
      foreignKey: 'userId'
    })
  };
  return Deck;
};