'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    const axios = require('axios')

    let insertion = []

    await axios({
      method: 'get',
      url: 'https://api.clashroyale.com/v1/cards',
      headers: {
        'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6Ijk1ODc2MmFiLWYyZTctNGQyNi1iOGZhLTIzMDlkODhlNDY0MCIsImlhdCI6MTU4ODYxNTg4Nywic3ViIjoiZGV2ZWxvcGVyL2I4ZjA0NTdiLWRiZWEtOWQ2ZC0xZGRlLTFlZWMyZDdmZGU1MiIsInNjb3BlcyI6WyJyb3lhbGUiXSwibGltaXRzIjpbeyJ0aWVyIjoiZGV2ZWxvcGVyL3NpbHZlciIsInR5cGUiOiJ0aHJvdHRsaW5nIn0seyJjaWRycyI6WyI5MC4xMjAuMTg3LjEyIiwiMTY0LjEzMi4yMjUuNzEiXSwidHlwZSI6ImNsaWVudCJ9XX0.mbYIVeLGLcev8zgp14Jlhi-hCHGgbbQKJJ-5mtgxxnliTU-tQB7EPRe4XXrGyv0P7zflRozbcYBXJNTbchsVEg'
      }
    }).then((response) => {
      //console.log(response['data'])

      response['data']['items'].forEach(card => {
        insertion.push({
          name: card['name'],
          elixir: Math.floor(Math.random() * 10 + 1),
          maxLevel: card['maxLevel'],
          iconUrl: card['iconUrls']['medium'],
          createdAt: new Date(),
          updatedAt: new Date()
        });
      });
    })

    return queryInterface.bulkInsert('Cards', insertion);
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('Cards', null, {});
  }
};
