const express = require('express');
const router = express.Router();

const db = require('../models')

/* GET list decks */
router.get("/", async (req, res, next) => {

    try {

        let options = {
            where: {
                userId: req.user.id
            }
        }

        if (req.query.limit) {
            req.query.offset = req.query.offset ? req.query.offset : 0

            options.limit = req.query.limit
            options.offset = req.query.offset
        }

        const decks = await db.Deck.findAndCountAll(options)
        const deckCards = await decks.cards
        console.log(decks)

        res.format({

            html: () => {
                res.render("deck/list", {
                    session: req.session,
                    user: req.user,
                    title: "Liste de decks",
                    count: decks.count,
                    decks: decks.rows,
                    deckCards: deckCards
                })
            },

            json: () => {
                res.json(decks)
            }
        })

    } catch (Err) {
        next(Err)
    }
})

/* GET add decks */
router.get("/add", async (req, res, next) => {

    const cards = await db.Card.findAll()

    res.render("deck/form", {
        session: req.session,
        user: req.user,
        cards: cards,
        title: "Ajouter un deck",
        isNew: true
    })
})

/* GET edit deck */
router.get("/:deckId/edit", async (req, res, next) => {

    try {

        const deck = await db.Deck.findByPk(req.params.deckId)
        const cards = await db.Card.findAll()
        const deckCards = await deck.cards

        if (deck.userId !== req.user.id) {
            throw new Error("Id du deck invalide")
        }

        res.render("deck/form", {
            session: req.session,
            user: req.user,
            title: "Ajouter un deck",
            deck: deck,
            cards: cards,
            deckCards: deckCards,
            isNew: false
        })

    } catch (Err) {
        next(Err)
    }

})

/* GET show deck */
router.get("/:deckId", async (req, res, next) => {

    try {

        const deck = await db.Deck.findByPk(req.params.deckId)
        const cards = await deck.cards
        await deck.cards

        if (deck.userId !== req.user.id) {
            throw new Error("Id du deck invalide")
        }

        res.format({

            html: () => {
                res.render("deck/show", {
                    title: deck.title,
                    deck: deck,
                    cards: cards,
                    session: req.session,
                    user: req.user
                })
            },

            json: () => {
                res.json({ deck, cards })
            }
        })

    } catch (Err) {
        next(Err)
    }
})

/* POST add deck */
router.post("/", async (req, res, next) => {

    try {

        const title = req.body.title ? req.body.title : "Sans titre"

        const nameCards = []
        for (let i = 0; i < 8; i++) {
            nameCards.push(req.body['card' + (i + 1)])
        }

        if (nameCards.indexOf('-- Sélectionner --') > -1) {
            throw new Error('Veuillez sélectionner toutes les cartes')
        }

        const deckCards = await db.Card.findAndCountAll({
            where: {
                name: nameCards
            }
        })

        if (deckCards.count != 8) {
            throw new Error('Veuillez sélectionner 8 cartes différentes')
        }

        let cardsValues = []
        let elixirAvr = 0
        deckCards.rows.forEach(card => {
            cardsValues.push(JSON.stringify(card.dataValues))
            elixirAvr += card.dataValues.elixir
        });
        elixirAvr /= 8

        const deck = await db.Deck.create({
            title: title,
            cards: cardsValues,
            elixirAvr: elixirAvr.toFixed(1)
        })

        await deck.setUser(req.user)

        res.format({

            html: function () {
                res.redirect('/decks')
            },

            json: function () {
                res.json(deck)
            }
        })

    } catch (Err) {
        next(Err)
    }
})

/* DELETE deck */
router.delete("/:deckId", async (req, res, next) => {

    try {

        const result = await db.Deck.destroy({
            where: {
                id: req.params.deckId,
                userId: req.user.id
            }
        }) ? {
                status: "success"
            } : {
                status: "failure"
            }

        res.format({

            html: () => {
                res.redirect('/decks')
            },

            json: () => {
                res.json(result)
            }
        })

    } catch (Err) {
        next(Err)
    }
})

/* PATCH deck (update) */
router.patch("/:deckId", async (req, res, next) => {

    try {

        let changes = {}
        let where = {
            where: {
                id: req.params.deckId,
                userId: req.user.id
            }
        }

        if (req.body.title) {
            changes.title = req.body.title
        }

        const nameCards = []
        for (let i = 0; i < 8; i++) {
            nameCards.push(req.body['card' + (i + 1)])
        }

        const deckCards = await db.Card.findAndCountAll({
            where: {
                name: nameCards
            }
        })

        if (deckCards.count != 8) {
            throw new Error('Veuillez sélectionner 8 cartes différentes')
        }

        changes.cards = []
        changes.elixirAvr = 0
        for (let i = 0; i < 8; i++) {
            let card = deckCards.rows[i].dataValues
            changes.cards.push(JSON.stringify(card))
            changes.elixirAvr += card.elixir
        }
        changes.elixirAvr /= 8
        changes.elixirAvr = changes.elixirAvr.toFixed(1)

        console.log(changes)

        const result = await db.Deck.update(changes, where) ?
            {
                status: "success"
            } : {
                status: "failure"
            }

        res.format({

            html: () => {
                res.redirect('/decks')
            },

            json: () => {
                res.json(result)
            }
        })

    } catch (Err) {
        next(Err)
    }
})


module.exports = router;