# API_ClashRoyale

## Lancement du projet en local

```
npm install

npm start
```

Vous pouvez maintenant vous rendre sur `http://localhost:3000/` pour accèder à l'application.  
  
Vous pouvez utiliser les migrations pour réinitialiser la database :
- avec `node_modules/.bin/sequelize db:migrate` pour lancer les migrations
- et `node_modules/.bin/sequelize db:migrate:undo:all` pour les annuler.

## Présentation API

Dans cette API nous avons mis au point un système d'authentification (inscription / connection) où l'utilisateur peut :
- Modifier ses données
- Supprimer son compte
- Créer son propre deck

Il y a également la possibilité de voir les cartes de Clash Royale et de les trier par rareté.

API réalisée avec express, pug, sequilize.